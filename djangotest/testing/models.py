from django.db import models

class Client(models.Model):
    name = models.CharField(max_length=100)
    address = models.TextField()
    email_address = models.EmailField()
    phone_number = models.IntegerField()

    def __str__(self):
        return self.name