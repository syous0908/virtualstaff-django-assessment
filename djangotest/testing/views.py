from django.db.models import Q
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.generic import ListView

from .forms import ClientForm
from .models import Client
from .filters import NameFilter


class ClientListView(ListView):
    model = Client
    template_name = 'testing/home.html'

    def get_context_data(self,  **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = NameFilter(self.request.GET, queryset=self.get_queryset())
        return context

def home(request):
    clients = Client.objects.all()
    return render(request, 'testing/home.html', {'clients': clients})




def create_client(request):
    form = ClientForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect('home')
    return render(request, 'testing/client-form.html', {'form': form})


def update_client(request, id):
    client = Client.objects.get(id=id)
    form = ClientForm(request.POST or None, instance=client)

    if form.is_valid():
        form.save()
        return redirect('home')
    return render(request, "testing/client-form.html", {'form': form, 'client': client})


def delete_client(request, id):
    client = Client.objects.get(id=id)

    if request.method == 'POST':
        client.delete()
        return redirect('home')
    return render(request, 'testing/client-delete.html', {'client': client})



