from django.urls import path
from . import views
urlpatterns = [
    path('', views.home, name= 'home'),
    path('new', views.create_client, name= 'create_client'),
    path('update/<int:id>/', views.update_client, name= 'update_client'),
    path('delete/<int:id>/', views.delete_client, name= 'delete_client'),


]
