from django import forms
from .models import Client

class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ['name','address','email_address','phone_number']

    def dupli(self):
        name = self.cleaned_data.get('name')
        for instance in Client.objects.all():
            if instance.name == name:
                raise forms.ValidationError('All ready taken')
        return name