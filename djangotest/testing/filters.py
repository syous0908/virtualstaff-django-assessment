import django_filters

from .models import *

class NameFilter(django_filters.FilterSet):
    class Meta:
        model = Client
        fields = ('name','email_address','phone_number')
